#belajar git

Repositori ini adalah Repositori untuk belajar GIT. Silakan baca materi berikut [git remote in Academy]
(https://www.notion.so/lumut)

Atau dapat memulai latihan Gitlab dari awal :
- [Bab 1 - Pendahuluan]
- [Bab 2 - Instalasi GIT]
- [Bab 3 - Membuat Repository Baru dalam Proyek]
- [Bab 4 - Simpan Perubahan Revisi dengan Git Commit]
- [Bab 5 - Melihat Catatan Log Revisi]
- [Bab 6 - Melihat Perbandingan Revisi dengan Git Diff]
- [Bab 7 - Perintah untuk Membatalkan Revisi]
- [Bab 8 - Menggunakan Percabangan untuk Mencegah Konflik]
- [Bab 9 - Perbedaan Git Checkout, Git Reset, Git Revert]
- [Bab 10 - Bekerja dengan Remote Repository]
- [Bab 11 - Menggunakan Git Pull dan Git Fetch]
